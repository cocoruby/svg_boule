#!/bin/bash

a_fill=(07c115 07c115 07c180 07c180 07c1b1 07c1b1 079ec1 079ec1 076ac1 076ac1 0745c1 0745c1 b6c107 c10b07)
a_stroke_width=(0.05 0.10 0.15 0.20 0.25 0.30 0.35)
a_stroke_color=(ffffff 000000)

while read p; do
   
  echo "$p" | sed "s/7cb9f5/${a_fill[$(( ( RANDOM % 14 ) ))]}/g" | sed "s/0.26458332/${a_stroke_width[$(( ( RANDOM % 7 ) ))]}/g" | sed "s/ffffff/${a_stroke_color[$(( ( RANDOM % 2) ))]}/g" >> carres_transformed_ter.svg
done < carres_fond.svg
