#!/bin/bash

a_fill=(07c115 07c115 07c180 07c180 07c1b1 07c1b1 079ec1 079ec1 076ac1 076ac1 0745c1 0745c1 b6c107 c10b07 000000 000000 000000 000000 000000 000000 000000 000000 000000 000000 000000 000000 000000 000000 000000 000000 000000 000000 000000 000000)
a_stroke_width=(0.05 0.10 0.15 0.20 0.25 0.30 0.35)
a_stroke_color=(ffffff 000000)

while read p; do

  fill=${a_fill[$(( ( RANDOM % ${#a_fill[@]} ) ))]}
  if [ $fill = "000000" ];
  then 
	  stroke_color=000000;
  else
	  #stroke_color=${a_stroke_color[$(( ( RANDOM % ${#a_stroke_color[@]}) ))]};
	  stroke_color=ffffff;
  fi
  
  echo "$p" | sed "s/7cb9f5/$fill/g" | sed "s/0.26458332/${a_stroke_width[$(( ( RANDOM % ${#a_stroke_width[@]} ) ))]}/g" | sed "s/ffffff/$stroke_color/g" >> carres_transformed_black.svg
done < carres_fond.svg
